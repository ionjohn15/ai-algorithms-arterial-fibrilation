# README #

AI algorithms for atrial fibrilation

### What is this repository for? ###

* AI algorithms for atrial fibrilation
* Version 1.0

### Trello ###

https://trello.com/b/8PVPNNwq/ai-algorithms-arterial-fibrilation

### Members ###

* Pintilie Andrei
* Tabarcea Augustus
* Cabac Dorina
* Cosmin Pascaru
* Crucerescu Ion

### Who do I talk to? ###

* Ion Crucerescu
* ion.crucerescu@movial.com