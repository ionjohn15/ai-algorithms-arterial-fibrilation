# State of The Art

# Andrei Pintilie #
3 Main approaches:
1. 1D-CNN based on ECG and PPG [https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6592499/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6592499/)
Background: Multiple methods were tried, support vector machine (SVM), artificial neural network, and k-nearest neighbor, with no really great practical application.
Data manipulation : Used both PPG and ECG data. https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6592499/bin/mhealth\_v7i6e12770\_app1.pdf

Architecture: We used a 1-dimensional convolutional neural network (1D-CNN) and a recurrent neural network (RNN) as 2 DL architectures

2. A non linear transformation of the ECG data  [https://www.sciencedirect.com/science/article/abs/pii/S1386505698001385?via%3Dihub](https://www.sciencedirect.com/science/article/abs/pii/S1386505698001385?via%3Dihub) (no access to whole file )

In particular, we shall review [non-linear transformations](https://www.sciencedirect.com/topics/computer-science/nonlinear-transformation) of the ECG, the use of [principal component analysis](https://www.sciencedirect.com/topics/nursing-and-health-professions/principal-component-analysis) (linear and non-linear), ways to map the transformed data into _n_-dimensional spaces, and the use of [neural networks](https://www.sciencedirect.com/topics/computer-science/neural-networks) (NN) based techniques for ECG pattern recognition and classification. The problems we shall deal with are the QRS/PVC recognition and classification, the recognition of ischemic beats and episodes, and the detection of [atrial fibrillation](https://www.sciencedirect.com/topics/nursing-and-health-professions/atrial-fibrillation).

3. Detailed CNN approach from single lead ECG with good description on ann architecture and hyperparameters [https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&amp;arnumber=8428414](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&amp;arnumber=8428414)

Some medical aspects related with AF detection
However, AF detection remains problematic due to its episodic pattern. In this paper, a multiscaled fusion of deep convolutional neural network (MS-CNN) is proposed to screen out AF recordings from single lead short electrocardiogram (ECG) recordings.
ECG data is only valid as it respects triangle ofEinthoven. It&#39;s hard to create new data because the order and orientation of ECG data matters. ECG data can be translated to numerical values and must respect very strict rules about the curvature and amplitude of every spike.

# Cosmin Pascaru #

Automated electrocardiogram (ECG) interpretations may be erroneous, and lead to erroneous overreads, including for atrial fibrillation (AF). There have been a number of cases where ECGs were misinterpreted, by both automatic algorithms and a human supervisor, which resulted in unnecessary treatment given to real patients.

The most common type of ECG is the 12-lead resting ECG, taken over a short period of time. Among the other types, the Holter monitor is also widely used. The Holter ECG is taken with a single lead over a long time (24 hours+).

There are some open ECG datasets, some examples are **MIT-BIH arrhythmia database (MITDB)** and **MIT-BIH Atrial Fibrillation**. However, many researchers choose to test their models on custom datasets, taken either from a single institution / timeframe, and sometimes choosing only a subset of the available data, due to certain restrictions. For example, [Smith et. al](https://doi.org/10.1016/j.ijcha.2019.100423) specifically use only the ECG examples that are classified as positive for the condition by either algorithm ( **Veritas** or **Cardiologs** ). The reason they did this is because they had experts decide on the true result of each ECG tested.

Most of the papers do some kind of domain-specific feature extraction from the ECGs before processing them, such as QRS detection and PQRS morphology ([Plesinger et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/30102251/?i=3&amp;from=/30524091/related)), RR intervals of the QRS-waves ([Kong et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/31319947/?i=8&amp;from=machine%20learning%20atrial%20fibrilatiom)), features based on Inter Beat Intervals ([Rasmus et al.](https://ieeexplore.ieee.org/document/8037253)), or others.

The models used also vary in many ways. Some use CNNs (convolutional neural networks) or simpler DNNs (deep neural networks) ([Smith et. al](https://doi.org/10.1016/j.ijcha.2019.100423), [Plesinger et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/30102251/?i=3&amp;from=/30524091/related)), some use SVMs or variations of it (IRBF-RVM, [Kong et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/31319947/?i=8&amp;from=machine%20learning%20atrial%20fibrilatiom)), or even bagged tree ensembles ([Plesinger et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/30102251/?i=3&amp;from=/30524091/related)).

Because of the differences in datasets, it is very hard to compare the results obtained by different papers. Nevertheless, the state-of-the-art accuracy seems to be around 90% ([Smith et. al](https://doi.org/10.1016/j.ijcha.2019.100423), [Plesinger et al.](https://www.ncbi.nlm.nih.gov/m/pubmed/30102251/?i=3&amp;from=/30524091/related)), with specificity being as high as 99.5% (a very small number of false positives). However, the state of the art sensitivity is much lower, meaning the algorithms still predict a significant number of false negatives.

# Augustus Tabarcea #

_Articles:_

- _An artificial intelligence-enabled ECG algorithm for the identification of patients with atrial fibrillation during sinus rhythm: a retrospective analysis of outcome prediction_

_Conclusions:_

- _Full article not available free_
- _Used standard 10-second, 12-lead ECGs as data_
- _patients aged 18 years or older with at least one digital, normal sinus rhythm, standard 10-second, 12-lead ECG_
- _ labels validated by cardiologist supervision_
- _Train, validation, test split = [7,1,2]_

- _Metrics used:_
  - _ AUC for validation_
  - _probability threshold from validation was used on test_
  - _AUC, Acc, Sensitivity, Specificity, F1 score_

_Other findings are the same as the document submitted by Crucerescu Ion__._

Terms: atrial dysrhythmias (AD), atrial tachycardia (AT), atrial flutter (AFL), atrial fibrillation (AF)

Other findings:

- Models:
  - 13 Conv + 3 Denes ([https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6737299/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6737299/))
- Metrics:
  - Acc 91.2%, specificity 99.5% ([https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6737299/](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6737299/))

Kaggle:

- Dataset:
  	- MIT DB:
    	- Last column labeled 0-4. Class 0 represents normal condition.
      		- N:0, S:1, V:2, F:3, Q:4
      		- N: Normal beat
			- S: Supraventricular premature beat
			- V: Premature ventricular contraction
			- F: Fusion of ventricular and normal beat
			- Q: Unclassifiable beat
  	- PTB:
    	- 2 labels
    	  - Normal 
		  - Abnormal

 	 - Keggle:
     	 - N:0, S:1, V:2, F:3, Q:4
     	 - N: Normal beat
			- S: Supraventricular premature beat
			- V: Premature ventricular contraction
			- F: Fusion of ventricular and normal beat
			- Q: Unclassifiable beat
  	     - 87554 train individuals
     	    - 187-element vectors containing the ECG lead II signal for one heartbeat
  	     - Use numpy loadtxt, or pandas to open the CSV files
 	     - 10506 abnormal invidivduals
  	     - 4046 normal individuals
  	     - 21892 test individuals
  	     - class distribution: **90589** , 2779, 7236, 803, 8039
- Models:
	- [https://github.com/dave-fernandes/ECGClassifier]
		- CNN and RNN:
			- batch size of 200
			- 8000 parameter updates
			- Adam
		- BNN:
			- 3.5M parameter updates
			- Batch of size 125
		- Conv 1D:
			- Inspiration: [https://arxiv.org/pdf/1805.00794.pdf](https://arxiv.org/pdf/1805.00794.pdf)
			- Architecture:
				- An initial 1-D convolutional layer
				- 5 repeated residual blocks (containing two 1-D convolutional layers with a passthrough connection and same padding; and a max pool layer)
				- A fully-connected layer
				- A linear layer with softmax output
				- No regularization was used except for early stopping
				- [https://ibb.co/60DYsrw]
		- RNN Model:
			- Two stacked bidirectional GRU layers (input is masked to the variable dimension of the heartbeat vector)
			- Two fully-connected layers connected to the last output-pair of the downstream (bidirectional) GRU layer
			- A linear layer with softmax output
			- Dropout regularization was used for the GRU layers
			- [https://ibb.co/xJq3YK6]
		- Bayesian Network:
			- Conv1D describe above, but the weights were stochastic, and posterior distributions of weights were trained using the Flipout method [(Wen, Vicol, Ba, Tran, &amp; Grosse, 2018)](https://arxiv.org/abs/1803.04386)
			- [https://ibb.co/8gVw5L1]
	- [https://www.kaggle.com/coni57/model-from-arxiv-1805-00794]
		- Conv1D + Adam, lr = 0.001, B1 = 0.9, B2 = 0.999
		- [https://ibb.co/bJSvDyQ]

# Ion Crucerescu #

**Elsevier Ltd**

**Convolutional neural network** to detect the electrocardiographic signature of atrial fibrillation present during normal sinus rhythm using standard 10-second, 12-lead ECGs.

- Allocated ECGs to the training, internal validation, and testing datasets in a 7:1:2 ratio.
- Calculated the area under the curve (AUC) of the receiver operatoring characteristic curve for the internal validation dataset to select a probability threshold, which we applied to the testing dataset.
- Evaluated model performance on the testing dataset by calculating the AUC and the accuracy, sensitivity, specificity, and F1 score with two-sided 95% CIs.

**Results:** 3051 (8·4%) patients in the testing dataset had verified atrial fibrillation before the normal sinus rhythm ECG tested by the model. A single AI-enabled ECG identified atrial fibrillation with an AUC of 0·87 (95% CI 0·86–0·88), sensitivity of 79·0% (77·5–80·4), specificity of 79·5% (79·0–79·9), F1 score of 39·2% (38·1–40·3), and overall accuracy of 79·4% (79·0–79·9). Including all ECGs acquired during the first month of each patient&#39;s window of interest (ie, the study start date or 31 days before the first recorded atrial fibrillation ECG) increased the AUC to 0·90 (0·90–0·91), sensitivity to 82·3% (80·9–83·6), specificity to 83·4% (83·0–83·8), F1 score to 45·4% (44·2–46·5), and overall accuracy to 83·3% (83·0–83·7).

**Resources:** 180 922 patients with 649 931 normal sinus rhythm ECGs for analysis: 454 789 ECGs recorded from 126 526 patients in the training dataset, 64 340 ECGs from 18 116 patients in the internal validation dataset, and 130 802 ECGs from 36 280 patients in the testing dataset.

[https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(19)31721-0/fulltext#seccestitle190](https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(19)31721-0/fulltext#seccestitle190)

# Dorina Cabac #

Artificial intelligence influences all aspects of human life. It is used in various fields including medicine. Though there are obvious limitations of AI, but studies show that this new technology has the ability to improve the prognosis and should become a staple in the medical community. Now we are trying to develop a program based on machine learning that will identify patients with atrial fibrillation, as this would significantly reduce costs. In this sense, an electrocardiograph with artificial intelligence (AI) using a neural network has been developed. The performance of the model was evaluated on a test data set, calculating AUC (area under the curve) and 95% accuracy was obtained. An AI-enabled ECG acquired during normal sinus rhythm permits identification at point of care of individuals with atrial fibrillation.

One of the ways to implement such a software was the  CNN model which uses the software Python 3.5 on the Keras library (Tensor-Flow background) was used. Then, the implemented deep CNN model was trained and evaluated using graphics processing unit (GeForce GTX1080 TI; Nvidia, Santa Clara, CA, USA) computing in a Windows 10 environment. The results show the possibility of automatically predicting AF based on the CNN model using a short-term normal ECG signal.