from keras.datasets import cifar10
from keras.utils import to_categorical
from keras.models import load_model
import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder

def two_labels(x):
    if x>0:
        return 1
    return 0

def predict(input):

    model = load_model("my_model.h5")
    scores = model.predict(input)
    print(scores)
    label_results =[ np.argmax(i) for i in scores]

    counts = np.bincount(label_results)
    result = np.argmax(counts)
    print(result)
    return result

if __name__ == '__main__':
    # Load the dataset
    ##not necessary
    test_df=pd.read_csv('ecg_data/normal1.csv',header=None)
    X_test = test_df.iloc[:, :187].values[0:9]
    X_test = X_test[:,:,np.newaxis]

    print(X_test.shape)
    predict(X_test)

