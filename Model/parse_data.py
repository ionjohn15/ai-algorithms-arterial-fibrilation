import pandas as pd
from sklearn.utils import resample
import pickle
train_df=pd.read_csv('ecg_data/mitbih_train.csv',header=None)
test_df=pd.read_csv('ecg_data/mitbih_test.csv',header=None)

df_1=train_df[train_df[187]==1]
df_2=train_df[train_df[187]==2]
df_3=train_df[train_df[187]==3]
df_4=train_df[train_df[187]==4]
df_0=(train_df[train_df[187]==0]).sample(n=20000,random_state=42)

df_1_upsample=resample(df_1,replace=True,n_samples=20000,random_state=123)
df_2_upsample=resample(df_2,replace=True,n_samples=20000,random_state=124)
df_3_upsample=resample(df_3,replace=True,n_samples=20000,random_state=125)
df_4_upsample=resample(df_4,replace=True,n_samples=20000,random_state=126)

train_df=pd.concat([df_0,df_1_upsample,df_2_upsample,df_3_upsample,df_4_upsample])

pickle.dump(train_df,open("train_df.pkl","wb"))
pickle.dump(test_df,open("test_df.pkl","wb"))

print("Finished")
