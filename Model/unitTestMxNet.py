import unittest
import mxNetModule
import pandas as pd
import numpy as np

class TestNetInput(unittest.TestCase):

    def test_read_pickle_files_withValid_input(self):
        train_pkl = "train_df.pkl"
        test_pkl = "test_df.pkl"

        train_df,test_pkl=mxNetModule.load_data(train_pkl,test_pkl)
        self.assertTrue(len(train_df)>0)
        self.assertTrue(len(test_pkl) > 0)

    def test_read_pickle_files_withInvalid_input(self):
        train_pkl = "something"
        test_pkl = "not something"

        response=mxNetModule.load_data(train_pkl,test_pkl)
        self.assertEqual(response,"No Path")

    def test_read_pickle_files_withInvalid_extension(self):
        train_pkl = "train_df.txt"
        test_pkl = "test_df.txt"

        response = mxNetModule.load_data(train_pkl, test_pkl)
        self.assertEqual(response, "No Path")

    def test_parse_data_valid_input(self):
        train_df= pd.DataFrame(np.random.randint(0,100,size=(20000, 188)))
        test_df=pd.DataFrame(np.random.randint(0,100,size=(20000, 188)))

        tuple=mxNetModule.parse_data(train_df,test_df)
        self.assertEqual(len(tuple),4)

    def test_parse_data_with_small_and_big_lenght(self):
        train_df= pd.DataFrame(np.random.randint(0,100,size=(20000, 50)))
        test_df=pd.DataFrame(np.random.randint(0,100,size=(20000, 200)))

        tuple=mxNetModule.parse_data(train_df,test_df)
        self.assertIsNone(tuple)

    def test_parse_data_for_correct_output_shape(self):
        train_df= pd.DataFrame(np.random.randint(0,100,size=(7000, 188)))
        test_df=pd.DataFrame(np.random.randint(0,100,size=(7000, 188)))

        tuple=mxNetModule.parse_data(train_df,test_df)
        self.assertEqual(tuple[0].shape,(7000,186))
        self.assertEqual(tuple[1].shape, (7000, 186))
        self.assertEqual(tuple[2].shape, (7000,))
        self.assertEqual(tuple[3].shape, (7000,))

    def test_parse_data_for_numeric_values_given_string(self):
        train_df= [['string']*188]*7000
        test_df=['o'*188]*7000

        tuple=mxNetModule.parse_data(train_df,test_df)
        self.assertIsNone(tuple)

    def test_prepare_data_valid_case(self):
        x_train = pd.DataFrame(np.random.randint(0, 100, size=(7000, 188))).iloc[:, :186].values
        x_test = pd.DataFrame(np.random.randint(0, 100, size=(7000, 188))).iloc[:, :186].values
        y_train = pd.DataFrame(np.random.randint(0, 100, size=(7000,)))
        y_test = pd.DataFrame(np.random.randint(0, 100, size=(7000,)))

        tuple = mxNetModule.prepare_data(x_train, x_test,y_train,y_test)
        self.assertEqual(len(tuple),4)

    def test_prepare_data_invalid_shape(self):
        x_train = pd.DataFrame(np.random.randint(0, 100, size=(54,188))).iloc[:, :186].values
        x_test = pd.DataFrame(np.random.randint(0, 100, size=(7000, 188))).iloc[:, :186].values
        y_train = pd.DataFrame(np.random.randint(0, 100, size=(7000,)))
        y_test = pd.DataFrame(np.random.randint(0, 100, size=(54,)))

        tuple = mxNetModule.prepare_data(x_train, x_test,y_train,y_test)
        self.assertIsNone(tuple)



