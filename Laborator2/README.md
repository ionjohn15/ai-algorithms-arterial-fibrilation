# Requirement analysis. UML Diagrams #

### Requirement analysis ###

[https://docs.google.com/spreadsheets/d/1MZ2i1ifXs8i-jEOZRsCQoYA2hrQ3BD03q4Bz7Yeba8s/edit?usp=sharing]

### Neural Network Architecture ###
![alternativetext](https://bitbucket.org/icrucerescu/ai-algorithms-arterial-fibrilation/raw/297826e092a1c74f9f3a8ce5cfcbd7fe5782f2b1/architecture.png?at=master)
### Future component diagram ###
![alternativetext2](https://bitbucket.org/icrucerescu/ai-algorithms-arterial-fibrilation/raw/297826e092a1c74f9f3a8ce5cfcbd7fe5782f2b1/diagram.png?at=master)